import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class DiferenciaDiagonal {
    /**
     * Diferencia diagonal.
     *
     * @param arr
     * @return
     */
    static int diagonalDifference(int[][] arr) {
        //En esta variable almacenaremos la suma de la primera diagonal
        int diagonal1 = 0;
        //En esta variable almacenaremos la suma de la segunda diagonal
        int diagonal2 = 0;
        //En esta variable almacenamos el número de renglones de la matriz
        //Ya que es una matriz cuadrada el número de columnas y filas es el mismo
        int n = arr.length;
        for (int i = 0; i < arr.length; i++) {
            diagonal1 += arr[i][i];
            diagonal2 += arr[i][n-1-i];
        }
        //Valor absoluto de la resta de la suma de la diagonal1 y diagonal2
        return Math.abs(diagonal1 - diagonal2);
    }

    /**
     * Imprime matriz.
     *
     * @param matrix
     */
    static void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                //\t es tabulador
                System.out.print(matrix[i][j] + "\t");
            }
            System.out.println("");
        }
        System.out.println("");
    }

    public static void main(String[] args) throws IOException {
        //Declaramos la matriz principal
        int n = 6;
        int m = 6;
        int matriz[][] = new int[n][m];
        //Llenamos la matriz principal de números "aleatorios" e imprimimos la matriz
        System.out.println("Matriz original:");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                matriz[i][j] = (int) (Math.random() * 100) + 1;
            }
        }
        printMatrix(matriz);
        int result = diagonalDifference(matriz);
        System.out.println("Suma diagonal: " + result);
    }
}
