import java.awt.*;

public class Transpuesta {

    public static void main(String[] args) {
        int n = 5;
        int m = 6;
        //Declaramos la matriz principal
        int matriz[][] = new int[n][m];
        int matrizTranspuesta[][] = new int[m][n];
        //Llenamos la matriz principal de números "aleatorios" e imprimimos la matriz
        System.out.println("Matriz original:");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                matriz[i][j] = (int) (Math.random() * 100) + 1;
                System.out.print(matriz[i][j]  + "\t");
            }
            System.out.println("");
        }
        System.out.println("Matriz transpuesta:");
        //Imprimimos la matriz transpuesta
        for (int j = 0; j < m; j++) {
            for (int i = 0; i < n; i++) {
                //Almacenamos la matriz transpuesta
                matrizTranspuesta[j][i] = matriz[i][j];
                System.out.print(matriz[i][j]  + "\t");
            }
            System.out.println("");
        }
    }
}
